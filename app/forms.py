from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField, HiddenField, validators
from wtforms.validators import DataRequired


class IndexForm(FlaskForm):
    member_id = StringField('member_id', validators=[DataRequired()])

class RegisterToVoteForm(FlaskForm):
    hidden = HiddenField()

    register = SubmitField(label='Send My Registration Email')
    cancel = SubmitField(label='Cancel Voting Request')


class MemberNotFoundForm(FlaskForm):
    hidden = HiddenField()
