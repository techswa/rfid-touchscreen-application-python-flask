# README #

## This is a project created for a local non-profit.##

### The solution allows members to swipe their RFID tag and display their current membership voting rights and privileges for 30 seconds before resetting to serve the next inquiry.
* The platform is a Raspberry Pi with a LCD touchscreen and attached RFID reader. 
* It consumes a JSON API connected to a Active Directory server containing membership records.
* Each member's record is derived by reading their RFID keychain dongle's 12-byte serial code and querying the backend server via the custom JSON API.
* The returned JSON is then parsed, injected into a web page, styled via SCSS and displayed on the LCD touch screen.
* The system also checks to verify that the member is in good standing and registered to vote in organizational elections.  In the case, the member isn't currently registered, the system prompts them to register, and if they agree to, an email is then generated and sent to the voter member committee.